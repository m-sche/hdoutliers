from hdoutliers import HDoutliers

# Asserted values are the output of R HDoutliers package

def test_pandas():
	import pandas as pd
	X = pd.read_csv('datasets/ex2d.csv');

	hdo = HDoutliers()
	out = hdo.fit_predict(X)
	outliers = {489, 500, 501, 502, 503, 504, 505, 506, 508, 509}
	assert {i for i in range(len(out)) if out[i] < 0} == outliers

def test_clustering():
	import pandas as pd
	X = pd.read_csv('datasets/ex2d.csv');

	hdo = HDoutliers(maxrows=500)
	out = hdo.fit_predict(X)
	outliers = {0, 20, 30, 75, 123, 165, 294, 358, 374, 380, 426, 449, 9, 15, 50, 59, 101, 161, 182, 223, 228, 243, 282, 303, 311, 314, 336, 348, 417, 428, 493, 34, 43, 57, 100, 280, 368, 412, 451, 39, 108, 139, 507, 49, 71, 267, 80, 359, 120, 299, 309, 334, 386, 448, 216, 396, 489, 500, 501, 502, 503, 504, 505, 506, 508, 509}
	assert {i for i in range(len(out)) if out[i] < 0} == outliers

def test_sparse():
	from scipy.sparse import csr_matrix
	import numpy as np
	X = csr_matrix(np.genfromtxt('datasets/sparse.csv', delimiter=','))

	hdo = HDoutliers()
	out = hdo.fit_predict(X)
	outliers = {9, 16, 17, 19}
	assert {i for i in range(len(out)) if out[i] < 0} == outliers

def test_numpy():
	import numpy as np
	X = np.genfromtxt('datasets/moon.csv', delimiter=',', skip_header=1)

	hdo = HDoutliers()
	out = hdo.fit_predict(X)
	outliers = {0, 1, 3, 22, 25, 35, 50, 66, 76, 82, 89, 98, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 111, 112, 115, 117, 118, 119, 120, 121, 122, 123, 124}
	assert {i for i in range(len(out)) if out[i] < 0} == outliers

def test_categorical():
	import pandas as pd
	X = pd.read_csv('datasets/categorical.csv');

	hdo = HDoutliers()
	out = hdo.fit_predict(X)
	outliers = {100}
	assert {i for i in range(len(out)) if out[i] < 0} == outliers
