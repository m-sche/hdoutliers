"""
Implementation of L. Wilkinson's HDoutliers algorithm for outlier detection.

Reference paper: <https://www.cs.uic.edu/~wilkinson/Publications/outliers.pdf>
"""

from scipy.sparse import issparse
import numpy as np
import mca
from sklearn.base import BaseEstimator
from sklearn.utils.validation import check_is_fitted
from pandas.api.types import is_numeric_dtype
import pandas as pd
from sklearn.random_projection import GaussianRandomProjection
from sklearn.neighbors import NearestNeighbors

class HDoutliers(BaseEstimator):
	"""
	HDoutliers algrorithm for outlier detection.

	Parameters
	----------
	eps : float (default=0.2)
	    If number of columns is greater than maxcols, random projection is used to
	    reduce dimensionality. Eps is a maximal relative error, which determines
	    the target number of dimensions.

	alpha : float (default=0.05)
	    Significance level of the test. Greater values of alpha causes more points
	    to be labeled as outliers.

	maxrows : int (default=10000)
	    If the data has more than maxrows rows, a clustering algorithm is used to
	    reduce complexity. The clustering takes O(n_samples) time.

	maxcols : int (default=10000)
	    If the dataset has more than maxcols columns, random projection is used
	    to reduce dimensionality.

	random_state : int, RandomState instance or None, (default=None)
	    A random seed used in random projection taking place if the number of
	    columns is greater than maxcols. If None, a random value is used.

	Attributes
	----------
	xid_ : int
	    Unique identifier of the data

	outlier_factor_ : array, shape (n_samples,)
	    Element at position i represents the outlier score of i-th input row,
	    the higher, the more outlierish. Minimal value is zero.

	cutpoint_ : float
	    Datapoints with higher outlier factor than this cutpoint are outliers

	Examples
	--------
	Load a dataset as a numpy matrix and visualize outliers:
	    import numpy as np
	    import matplotlib.pyplot as plt
	    X = np.genfromtxt('datasets/moon.csv', delimiter=',', skip_header=1)
            outliers = HDoutliers().fit_predict(X)
            plt.scatter(X[:,0], X[:,1], c=np.where(outliers > 0, 'C0', 'C1'))
            plt.show()
	
	Load a dataset with categorical variables and print the row IDs of the outliers
	    import numpy as np
	    import pandas as pd
	    df = pd.read_csv('datasets/categorical.csv');
	    outliers = HDoutliers().fit_predict(df)
	    print({i for i in range(len(outliers)) if outliers[i] < 0})

	Remove the outliers from a sparse matrix:
	    from scipy.sparse import csr_matrix
	    X = csr_matrix(np.genfromtxt('datasets/sparse.csv', delimiter=','))
            clean_data = HDoutliers().fit_transform(X)

	References
	----------
	.. [1] L. Wilkinson, "Visualizing Big Data Outliers Through Distributed
	       Aggregation," in IEEE Transactions on Visualization and Computer
	       Graphics, vol. 24, no. 1, pp. 256-266, Jan. 2018.
	       http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=8019881&isnumber=8165924

	"""

	def __init__(self, eps=0.2, alpha=0.05, maxrows=10000, maxcols=10000, random_state=None):
		self.eps = eps
		self.alpha = alpha
		self.maxrows = maxrows
		self.maxcols = maxcols
		self.random_state = random_state
	
	def fit(self, X, y=None):
		"""
		Fit the model with data X.

		Parameters
		----------
		X : array-like or sparse matrix, shape (n_samples, m_features)
		    If the data is an instance of Pandas DataFrame, categorical values are
		    converted to continuous variables. Otherwise, the matrix can contain
		    only numeric values. Missing values are not allowed.

		y : Always ignored

		Returns
		-------
		self : object
		    Returns the instance of self
		"""

		if len(X.shape) != 2:
			raise ValueError('Expected a 2D array, sparse matrix or a pandas DataFrame')

		self.xid_ = id(X)

		#  1. If there are any categorical variables in the dataset, convert each categorical
		#     variable to a continuous variable by using Correspondence Analysis.

		if isinstance(X, pd.DataFrame) and np.any([not is_numeric_dtype(X[col]) for col in X]):
			# In order not to modify original X, a deep copy
			# has to be made if categorical variables are present
			Xnum = X.copy(deep=True)
			for col in Xnum:
				if not is_numeric_dtype(Xnum[col]):
					mca_col = mca.MCA(Xnum, [col], benzecri=False)
					Xnum[col] = mca_col.fs_r(N=1)
			X = Xnum

		if not issparse(X):
			X = np.asfarray(X)

		#  2. If there are more than 10,000 columns, use random projections to reduce
		#     the number of columns to p = 4 log n / (eps^2 / 2 - eps^3 / 3), where eps is
		#     the error bound on squared distances.

		if X.shape[1] > self.maxcols:
			X = GaussianRandomProjection(eps = self.eps, random_state=self.random_state).fit_transform(X)
		
		#  3. Normalize the columns of the resulting n by p matrix X.

		xmin = X.min(axis=0).toarray() if issparse(X) else X.min(axis=0)
		xmax = X.max(axis=0).toarray() if issparse(X) else X.max(axis=0)
		diff = xmax - xmin
		diff[diff == 0] = 1	# Prevent division by zero

		X = (X - xmin) / diff

		#  4. Let row(i) be the i-th row of X.
		#  5. Let delta = 0.1 / (log n)^(1 / p).

		delta = 0.1 / (np.log(X.shape[0]) ** (1 / X.shape[1]))

		#  6. Initialize exemplars, a list of exemplars with initial entry [row(1)].
		#  7. Initialize members, a list of lists with initial entry [1]; each exemplar will
		#     eventually have its own list of affiliated member indices.
		#  8. Now do one pass through X (the Leader algorithm)

		members = [None] * X.shape[0]

		if X.shape[0] <= self.maxrows:
			# No reduction is needed for small number of rows. Cluster only duplicates.

			_, clusters = np.unique(X, axis=0, return_inverse=True)
			exemplars = []
			cluster_map = {}
			
			for i, cl in enumerate(clusters):
				if cl in cluster_map:
					members[cluster_map[cl]].append(i)
				else:
					cluster_map[cl] = i
					exemplars.append(i)
					members[i] = [i]

		else:
			# For larger datasets, datapoints are clustered using the Leader algorithm

			exemplars = [0]
			members[0] = [0]
			
			for i in range(1, X.shape[0]):
				# Find the closest exemplar to the i-th datapoint
				knn = NearestNeighbors(n_neighbors=1, metric='l2').fit(X[exemplars])
				distance, closest = knn.kneighbors(X[i].reshape(1, -1), return_distance=True)

				distance = distance[0][0]
				closest = exemplars[closest[0][0]]

				# Assign datapoint to a cluster
				if distance < delta:
					members[closest].append(i)
				else:
					exemplars.append(i)
					members[i] = [i]

		#  9. Now compute nearest-neighbor distances between all pairs of exemplars in the exemplars list.

		knn = NearestNeighbors(n_neighbors=1, metric='l2').fit(X[exemplars])
		nndist = knn.kneighbors()[0].reshape(-1)

		# 10. Fit an Exponential distribution to the upper tail of the nearest-neighbor distances and
		#     compute the upper 1-alpha point of the fitted cumulative distribution function (CDF).

		# This part is directly adopted from the R code

		n = len(nndist)
		ordered = sorted(nndist)
		gaps = np.concatenate(([0], np.diff(ordered)))	# Derivative of the ECDF
		ghat = np.zeros(len(nndist))

		n4 = max(min(50, int(np.floor(n / 4))), 2)
		start = max(int(np.floor(n / 2)), 1)
		upperhalf = range(start, n)

		for i in upperhalf:
			# Expected value of the adjacent lower quarter of the derivated ECDF
			ghat[i] = sum(((j / n4) * gaps[i - j + 1]) for j in range(1, n4 + 1))

		self.cutpoint_ = float('inf')
		logAlpha = np.log(1 / self.alpha)
		for i in upperhalf:
			if gaps[i] > logAlpha * ghat[i]:
				self.cutpoint_ = ordered[i - 1]
				break
		
		# 11. For any exemplar that is significantly far from all the other exemplars based on this
		#     cutpoint, flag all entries of members corresponding to exemplar as outliers.

		self.outlier_factor_ = np.zeros(len(members))
		for ex, dist in enumerate(nndist):
			self.outlier_factor_[members[exemplars[ex]]] = dist

		return self

	def predict(self, X=None):
		"""
		Predict the labels (1 inlier, -1 outlier) of X according to HDoutliers.

		Parameters
		----------
		X : array-like or sparse matrix, shape = (n_samples, n_features) or None
		    The data must be identical to those used in fit() method or None.

		y : Always ignored.

		Returns
		-------
		is_inlier : array, shape (n_samples,)
		    Returns -1 for anomalies/outliers and +1 for inliers.
		"""

		check_is_fitted(self, 'outlier_factor_')
		if X is not None and id(X) != self.xid_:
			raise RuntimeError('Prediction data differ from fitted data!')

		return np.where(self.outlier_factor_ > self.cutpoint_, -1, 1)

	def fit_predict(self, X=None, y=None):
		"""
		Fit the model with data X and predict the labels (1 inlier, -1 outlier) of X
		according to HDoutliers.

		Equivalent to calling fit(X).predict(X)

		Parameters
		----------
		X : array-like or sparse matrix, shape (n_samples, m_features)
		    If the data is an instance of Pandas DataFrame, categorical values are
		    converted to continuous variables. Otherwise, the matrix can contain
		    only numeric values. Missing values are not allowed.

		y : Always ignored.

		Returns
		-------
		is_inlier : array, shape (n_samples,)
		    Returns -1 for anomalies/outliers and +1 for inliers.
		"""

		return self.fit(X).predict(X)
	
	def transform(self, X):
		"""
		Transform the dataset to contain only inliers.

		Parameters
		----------
		X : array-like or sparse matrix, shape = (n_samples, n_features)
		    The data must be identical to those used in fit() method.

		Returns
		-------
		X : array or csr_matrix, shape (n_inliers, n_features), n_inliers <= n_samples
		    The original data without outliers. Sparse data will be returned as
		    a csr_matrix, a pandas DataFrame keep the type, other data will be
		    returned as a numpy array.
		"""

		check_is_fitted(self, 'outlier_factor_')
		if id(X) != self.xid_:
			raise RuntimeError('Transform data differ from fitted data!')

		index = np.where(self.outlier_factor_ <= self.cutpoint_)[0]

		if issparse(X):
			return X.tocsr()[index, :]
		elif isinstance(X, pd.DataFrame):
			return X.iloc[index]
		else:
			return np.asarray(X)[index]

	def fit_transform(self, X, y=None):
		"""
		Fit the model with data X and remove outliers according to HDoutliers.

		Equivalent to calling fit(X).transform(X)

		Parameters
		----------
		X : array-like or sparse matrix, shape (n_samples, m_features)
		    If the data is an instance of Pandas DataFrame, categorical values are
		    converted to continuous variables. Otherwise, the matrix can contain
		    only numeric values. Missing values are not allowed.

		y : Always ignored.

		Returns
		-------
		X : array, shape (n_inliers, n_features) where n_inliers <= n_samples
		    The original data without outliers.
		"""

		return self.fit(X).transform(X)
