% Dokumentace k implementaci algoritmu HDoutliers v Pythonu
% Kompiluje se pomocí: pdfcsplain dokumentace.tex

\input opmac
\input cs-pagella
\typosize[12/14]
\activettchar"
\picw=.5\hsize

\centerline{\typosize[24/24] HDoutliers }

\sec Algoritmus HDoutliers

Algoritmus HDoutliers slouží k detekci outlierů (odlehlých hodnot) v datech. Je popsán v článku {\em Visualizing Big Data Outliers Through Distributed Aggregation}\fnote{\url{https://www.cs.uic.edu/~wilkinson/Publications/outliers.pdf}}, autorem je Leland Wilkinson.

Algoritmus umí pracovat s numerickými i kategorickými daty, ale neporadí si s chybějícími hodnotami. Princip algoritmu -- s důrazem na reálně implementované funkce v tomto balíčku -- je následující (nastavitelné parametry algoritmu jsou označeny neproporcionálním písmem):
\begitems\style X
* Kategorická data jsou pomocí MCA\fnote{MCA -- multiple correspondence analysis, technika podobná PCA, ale pro kategorická data} převedena do numerické reprezentace. Tento převod se provádí, pouze pokud jsou data ve formátu pandas DataFrame. Ostatní maticové reprezentace dat jsou apriori považovány za numerické.
* Pokud má dataset víc jak "maxcols" (defaultně 10\,000) sloupců, počet sloupců se sníží náhodnými projekcemi\fnote{Viz Wikipedia heslo "Random projection" nebo dokumentaci třídy "GaussianRandomProjection" z~ba\-líčku "sklearn"} tak, aby chyba této projekce nepřesahovala "eps" (defaultně $0{,}2 = 20\,\%$).
* Všechny (nyní už numerické) příznaky se přeškálují do rozsahu $\langle 0, 1\rangle$.
* Pokud je počet řádků větší než "maxrows" (defaultně 10\,000), data se zredukují do clusterů ${\cal O}(N)$ algoritmem. Každý cluster je reprezentován jedním představitelem (exemplar). Když je dat málo, clusterují se pouze identické body a každý bod je představitelem svého vlastního clusteru.
* Mezi clustery se spočítají Euklidovské vzdálenosti k nejbližšímu sousedovi. Předpokládá se, že budou mít exponenciální rozdělení. Myšlenka hledání outlierů je taková, že se provede se statistický test na to, které hodnoty jsou mimo toto roz\-dělení s hladinou významnosti $\alpha = $ "alpha", a ty se označí za outliery.
* Vzdálenosti k nejbližšímu sousedovi se seřadí od nejmenší po největší a spočítají se rozdíly mezi sousedními vzdálenostmi. Dostaneme analogii derivace empirické distribuční funkce, označme ji $g(n)$, kde $n\in N$ je index v rámci seznamu seřazeného podle NN vzdálenosti.
* Pro horní polovinu argumentů $g(n)$ spočítáme střední hodnotu předchozí čtvrtiny hodnot $g(n)$:
$$ \forall n\in N, n \ge {N\over 2}: \quad \hat g(n) := {\rm E}\,\big\{ g\left(n-{N/ 4}\right), g\left(n-{N/ 4} + 1\right), \cdots, g(n) \big\} $$
* Nejmenší $n\in N, n \ge {N\over 2}$, pro které platí
$$ g(n) > \ln\left(1 \over \alpha\right) \cdot \hat g(n) $$
označíme $n_{\rm cut}$.
* Všechny představitele clusterů na indexech $n \ge n_{\rm cut}$ označíme za outliery. Stejně tak označíme za outliery všechny původní datové body náležící do clusteru, jehož představitel je outlier. Pokud $n_{\rm cut}$ neexistuje, tj. pro žádné $n$ neplatí podmínka z předchozího bodu, všechna data jsou inlieři.
\enditems

\sec Použití

Třída a metody jsou podrobně zdokumentovány standardními docstringu, plnou do\-kumentaci lze tedy získat voláním "pydoc ./hdoutliers.py". Zde jsou uvedeny typické příklady použití s očekávanými výsledky

\secc Zobrazení outlierů v 2d numpy matici s dvěma příznaky

\begtt
import numpy as np
import matplotlib.pyplot as plt
X = np.genfromtxt('datasets/moon.csv', delimiter=',', skip_header=1)
outliers = HDoutliers().fit_predict(X)
plt.scatter(X[:,0], X[:,1], c=np.where(outliers > 0, 'C0', 'C1'))
plt.show()
\endtt

Výstup (outlieři jsou označeni oranžově):

\centerline{\inspic moon.pdf }

\secc Výpis outlierů v datasetu s kategorickými příznaky

\begtt
import numpy as np
import pandas as pd
df = pd.read_csv('datasets/categorical.csv');
outliers = HDoutliers().fit_predict(df)
print({i for i in range(len(outliers)) if outliers[i] < 0})
\endtt

Očekávaný výstup (jediný outlier má ID 100, indexy se počítají od nuly):
\begtt
{100}
\endtt

\secc Zobrazení outlierů v řídké matici s mnoha příznaky

\begtt
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import TruncatedSVD as SVD

X = csr_matrix(np.genfromtxt('datasets/sparse.csv', delimiter=','))
hdo = HDoutliers()
outliers = hdo.fit_predict(X)

svd = SVD(n_components=2)
tf = pd.DataFrame(svd.fit_transform(X))
plt.scatter(tf[0], tf[1], c=np.where(outliers > 0, 'C0', 'C1'))
plt.show()
\endtt

Výstup (outlieři jsou označeni oranžově):

\centerline{\inspic sparse.pdf }

\secc Porovnání původního datasetu s očištěnými daty

\begtt
import numpy as np
import matplotlib.pyplot as plt
df = pd.read_csv('datasets/ex2d.csv');
tf = HDoutliers().fit_transform(df)
plt.scatter(df['x'], df['y'])
plt.show()
plt.scatter(tf['x'], tf['y'])
plt.show()
\endtt

Výstup (nalevo původní dataset, napravo tentýž dataset bez outlierů):

\centerline{\inspic ex2d.pdf \inspic ex2d_tf.pdf }

\sec Implementační detaily

Algoritmus je implementován v Pythonu 3 ve třídě "HDoutliers". Tato třída implementuje následující "scikit-learn" rozhraní:
\begitems
* BaseEstimator -- Poskytuje metodu "fit", v případě HDoutliers ale nemá velký význam volat ji samostatně
* Predictor -- Metody "predict" a "fit_predict" klasifikují řádky datasetu na outliery a inliery
* Transformer -- Metody "transform" a "fit_transform" vrací dataset očištěný o outliery
\enditems
\noindent Doporučuje se používat metody "fit_predict", resp. "fit_transform", protože fitovaná data se musí shodovat s klasifikovanými, resp. transformovanými daty.

Kritické sekce kódu se drží referenční implementace algoritmu HDoutliers v R\fnote{\url{https://rdrr.io/cran/HDoutliers/}}. Tato implementace se snaží o maximální shodu výsledků právě s tímto R balíčkem. Oproti R je ale implementována i náhodná projekce při vysoké dimenzionalitě data\-setu, což R neřeší, ačkoli je tento krok popsán v článku.

Celá třída je implementována v souboru "hdoutliers.py", ten je jediný s funkčně užitečným kódem. Adresář "datasets" obsahuje několik .csv souborů s datasety růz\-ných vlastností, ty slouží zejména k rychlému testování funkčnosti. K~základnímu otestování funkčnosti slouží soubor "hdo_tests.py" obsahují několik testů spustitelných příkazem "pytest". Výstupy jsou porovnávány s výstupy referenční implementace v R. Testován je vstup ve formátech pandas DataFrame, numpy 2d array a scipy sparse matrix, data jsou dvou- až devítidimenzionální, numerická i smíšená (s kategorickými příznaky). Pytest hlásí jeden warning při testu s řídkou maticí, který se ale nevyskytuje při spuštění totožného kódu ručně. Testy vyžadují datasety přítomné v adresáři "datasets".



\bye
